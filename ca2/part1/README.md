PART 1
--------

1- criar a pasta “ca2” e as seguintes sub-pastas:
ca2/part1/luisnogueira-gradle_basic_demo-fef780d15427 (com os seus ficheiros).

---

2- No IntelliJ IDEA.
Fiz o import d

---

3- No TERMINAL.
cd devops-18-19-atb/

---

4- No TERMINAL.
- git add -A

---

5-  No TERMINAL.
- git commit -a -m “first commit”

---

6- No TERMINAL.
- git push origin master

---

7- Ver o ficheiro README

---

8- No TERMINAL.
- brew install gradle

Obtive esta informação:
Warning: You are using macOS 10.11.
We (and Apple) do not provide support for this old version.
You will encounter build failures with some formulae.
Please create pull requests instead of asking for help on Homebrew's GitHub,
Discourse, Twitter or IRC. You are responsible for resolving any issues you
experience, as you are running this old version.

---

9- No TERMINAL.
- ./gradle build.

---

Obtive esta informação:

Gradle 5.2.1


Build time:   2019-02-08 19:00:10 UTC
Revision:     f02764e074c32ee8851a4e1877dd1fea8ffb7183

Kotlin DSL:   1.1.3
Kotlin:       1.3.20
Groovy:       2.5.4
Ant:          Apache Ant(TM) version 1.9.13 compiled on July 10 2018
JVM:          11 (Oracle Corporation 11+28)
OS:           Mac OS X 10.11.6 x86_64

MacBook-Pro-de-Antonio-3:devops-18-19-atb antoniooliveira$ gradle build
Starting a Gradle Daemon (subsequent builds will be faster)

> Task :buildEnvironment


Root project


classpath
No dependencies

A web-based, searchable dependency report is available by adding the --scan option.

BUILD SUCCESSFUL in 17s
1 actionable task: 1 executed

---

10- No TERMINAL.

fui ao directorio projecto.

---

11- No TERMINAL. Segui as instruções do README.

11.1 - ./gradlew build

11.2 - % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

---

12- No TERMINAL. Abri uma nova janela. fui ao directorio projecto.

- ./gradlew runClient

---

13- No TERMINAL. Janela original. 

Obtive esta informação:

- 10:56:41.093 [pool-1-thread-1] INFO  basic_demo.ChatServer.Handler - A new user has joined: Antonio

---

14- No TERMINAL.

- git commit -a -m "chatworking"

e depois fiz 

- git push origin master

---

15- No INTELLIJ. Add a new task to execute the server.

Na pasta “src” no ficheiro “build.gradle”.

- task runServer(type:JavaExec, dependsOn: classes){
    classpath = sourceSets.main.runtimeClasspath
    main = 'basic_demo.ChatServerApp'
    args '59001'
}

---

16- No TERMINAL.

- ./gradlew build

---

17- No TERMINAL.

- % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

---

18- No TERMINAL. Em janelas diferentes.
./gradlew runClient

Criando novos utilizadores/nomes.

---

19- No INTELLIJ.

Criei as pastas (src/test/java/basic_demo) no projecto para criar a classe de teste com que requerem o “junit 4.12” para correr.

Copiei o método de “testAppHasAGreeting” para a class (AppTest) e fiz o teste com sucesso.

---

19- No TERMINAL.
- git commit -a -m "testAppHasAGreeting"
- git push origin master

---

20- No INTELLIJ.

No “build.gradle”

dependencies {
    // Use Apache Log4J for logging
    compile group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    compile group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
    testCompile ('junit:junit:4.12')
}

Foi colocado “testCompile ('junit:junit:4.12')”

21- No TERMINAL.

- ./gradlew test

Teste correu com sucesso.
BUILD SUCCESSFUL in 7s
4 actionable tasks: 2 executed, 2 up-to-date

22- No TERMINAL.

No “build.gradle”
Escrevi duas Task:

task Copy(type: Copy){
    from 'src'
    into 'backup'
    
}

task CopyTwo(type: Copy){
    from 'src'
    into 'backuptwo'
    exclude '**/*.md'
}

22- No TERMINAL.

corri no terminal com sucesso. E fez as copias.

- ./gradlew Copy

- ./gradlew CopyTwo

Deu em ambos este resultado:

BUILD SUCCESSFUL in 1s
1 actionable task: 1 executed

22- No TERMINAL.
- git commit -a -m "task_copy"
- git push origin master

23- No TERMINAL.

escrevi esta Task:

task Zip(type: Zip) {

    from 'src'
    
    archiveFileName = "scrzip.zip"
    destinationDirectory = file("backup")
}

e deu esta resposta: BUILD SUCCESSFUL in 1s

24- No TERMINAL.

- git commit -a -m "task_zip"
- git push origin master

25- No TERMINAL.

- git tag -a part.1.final -m "part one final"

- git push origin part.1.final


PART 2
--------

